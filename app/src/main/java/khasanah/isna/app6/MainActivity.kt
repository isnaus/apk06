package khasanah.isna.app6

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var  db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragBagi : FragmentPembagian
    lateinit var fragPer  : FragmentPerkalian
    //////////////////////////////////////////////
    lateinit var fragNilai : FragmentNilai
    lateinit var fragMk     : FragmentMatkul
    //////////////////////////////////////////////
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragBagi = FragmentPembagian()
        fragPer = FragmentPerkalian()
        ///////////////////////////////////////////////////////////////////
        fragNilai = FragmentNilai()
        fragMk    = FragmentMatkul()
        //////////////////////////////////////////////////////////////////
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemMatkul -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMk).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
                return true
            }
            R.id.itemNilai -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragNilai).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemBagi -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBagi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,225,255))
                frameLayout.visibility = View.VISIBLE
            }

            R.id.itemKali -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragPer) .commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE
        }
        return true
    }
}
