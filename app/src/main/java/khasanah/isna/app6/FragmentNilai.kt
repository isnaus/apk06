package khasanah.isna.app6

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.spMatkul

class FragmentNilai: Fragment(), View.OnClickListener{

    val MhsSelect = object : AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {
            spNama.setSelection(0, true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
           val c: Cursor = spAdapter2.getItem(position) as Cursor
            namaMhs = c.getString(c.getColumnIndex("_id"))
        }
    }
    val MatkulSelect = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spMatkul.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = spAdapter.getItem(position) as Cursor
            namaMkl = c.getString(c.getColumnIndex("_id"))
        }

    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var spAdapter2: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaMhs : String = ""
    var namaMkl : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v =  inflater.inflate(R.layout.frag_data_nilai,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnNilaiDelete.setOnClickListener(this)
        v.btnNilaiInsert.setOnClickListener(this)
        v.btnNilaiUpdate.setOnClickListener(this)
        v.spNama.onItemSelectedListener = MhsSelect
        v.spMatkul.onItemSelectedListener = MatkulSelect
        return v
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnNilaiDelete ->{

            }
            R.id.btnNilaiInsert ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnNilaiUpdate ->{

            }
        }
    }
    override fun onStart() {
        super.onStart()
        showDataMatkul()
        showDataMhs()
        showData()
    }

    fun showData(){
        var sql = "select nilai.nomer as _id , mhs.nama , matkul.namamk, nilai.nilai from nilai,mhs, matkul  " +
                " where nilai.nim = mhs.nim and nilai.kodemk = matkul.kodemk"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_nilai,c,
            arrayOf("_id","nama","namamk","nilai"), intArrayOf(R.id.txNo, R.id.txNama, R.id.txMatkul, R.id.txNilai),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lvNilai.adapter = lsAdapter
    }

    fun showDataMatkul(){
        val c : Cursor = db.rawQuery("select kodemk, namamk as _id, id_prodi from matkul", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMatkul.adapter = spAdapter
        v.spMatkul.setSelection(0)
    }
    fun showDataMhs(){
        val cd : Cursor = db.rawQuery("select nim, nama as _id ,  id_prodi from mhs", null)
        spAdapter2 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,cd,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spNama.adapter = spAdapter2
        v.spNama.setSelection(0)
    }

    fun insertDataNilai(nim: String, kodemk : String, nilai : String){
        var sql = "insert into nilai( nim, kodemk , nilai) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,kodemk,nilai))
        showData()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select nim from mhs where nama='$namaMhs'"
        var sql2 = "select kodemk from matkul where namamk='$namaMkl'"
        val c : Cursor = db.rawQuery(sql,null)
        val c1 : Cursor = db.rawQuery(sql2,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataNilai(
                c.getString(c.getColumnIndex("nim")),
                c1.getString(c1.getColumnIndex("kodemk")),
                v.edNilai.text.toString()
            )
            v.spNama.setSelection(0)
            v.spMatkul.setSelection(0)
            v.edNilai.setText("")
        }
    }
}