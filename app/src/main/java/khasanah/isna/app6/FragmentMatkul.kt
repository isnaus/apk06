package khasanah.isna.app6

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_matkul.*
import kotlinx.android.synthetic.main.frag_data_matkul.view.*

class FragmentMatkul : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener  {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spProdi.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaPrd = c.getString(c.getColumnIndex("_id"))
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaMk : String = ""
    var namaPrd : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_matkul,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnMkDelete.setOnClickListener(this)
        v.btnMkInsert.setOnClickListener(this)
        v.btnMkUpdate.setOnClickListener(this)
        v.spProdi.onItemSelectedListener = this
        return v

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMkDelete ->{

            }
            R.id.btnMkUpdate ->{

            }
            R.id.btnMkInsert ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showDataMatkul()
        showDataProdi()
    }

    fun showDataMatkul(){
//        var sql = "select mhs.nim as _id, mhs.nama, prodi.nama_prodi from mhs , prodi " +
//                    "where mhs.id_prodi = prodi.id_prodi order by mhs.nama asc"
//        val c : Cursor = db.rawQuery(sql,null)
//        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_matkul,c,
//            arrayOf("_id","nama","nama_prodi"), intArrayOf(R.id.txKodeMk, R.id.txNamaMk, R.id.txPrd),
//            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
//        v.lvMk.adapter = lsAdapter
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var sql = "select matkul.kodemk as _id , matkul.namamk , prodi.nama_prodi from matkul , prodi " +
                " where matkul.id_prodi = prodi.id_prodi order by _id asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_matkul,c,
            arrayOf("_id","namamk","nama_prodi"), intArrayOf(R.id.txKodeMk, R.id.txNamaMk, R.id.txPrd),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lvMk.adapter = lsAdapter
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        var sql = "select mhs.nim as _id, mhs.nama, prodi.nama_prodi from mhs , prodi " +
//                    "where mhs.id_prodi = prodi.id_prodi order by mhs.nama asc"
//        val c : Cursor = db.rawQuery(sql,null)
//        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_mahasiswa,c,
//            arrayOf("_id","nama","nama_prodi"), intArrayOf(R.id.txNimMhs,R.id.txNamaMhs,R.id.txNamaPS),
//            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
//        v.lvMk.adapter = lsAdapter
    }


    fun showDataProdi(){
        val c : Cursor = db.rawQuery("select nama_prodi as _id from prodi order by nama_prodi asc",null)
        spAdapter = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spProdi.adapter = spAdapter
        v.spProdi.setSelection(0)
    }

    fun insertDataMatkul(kode : String, namaMk: String, id_prodi : Int){
        var sql = "insert into matkul(kodemk, namamk, id_prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(kode,namaMk,id_prodi))
        showDataMatkul()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_prodi from prodi where nama_prodi='$namaPrd'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataMatkul(v.edKode.text.toString(), v.edNamaMk.text.toString(),
                c.getInt(c.getColumnIndex("id_prodi")))
            v.edKode.setText("")
            v.edNamaMk.setText("")
        }
    }
}